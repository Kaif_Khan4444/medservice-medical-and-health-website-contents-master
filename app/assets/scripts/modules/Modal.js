import $ from 'jquery';
class Modal {
    constructor() {
        this.openModalbuttons=$(".open-modal");
        this.modal=$(".modal");
        this.modalClosebutton= $(".modal-close");
        this.events();
    }
    events() {
        this.openModalButtons.click(this.openModal.bind(this));
         this.closeModalButtons.click(this.closeModal.bind(this));
         $(document).keyup(this.keyPressHandler.bind(this));
    }
    openModal() {
        this.modal.addClass("modal-is-visible");
        return false;
    }
    closeModal() {
        this.modalremoveClass("modal-is-visible");
    }
    keyPressHandler(e) {
        if(e.keyCode==27) {
            this.closeModal();
        }
    }
    }
export default Modal;
